package com.libraryapp.demo.controller;

import java.util.Arrays;
import java.util.List;
import java.lang.reflect.Type;


import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.libraryapp.demo.EntityNotFoundException;
import com.libraryapp.demo.config.Dto;
import com.libraryapp.demo.dto.AuthorCreationDTO;
import com.libraryapp.demo.dto.AuthorDTO;
import com.libraryapp.demo.dto.AuthorUpdateDTO;
import com.libraryapp.demo.entity.Author;
import com.libraryapp.demo.service.IAuthorService;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;



import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;





@RestController
@Api(tags = "Author Endpoint",value = "Author Management System", description = "Operations pertaining to author in Library Management System")
@RequestMapping(path = "/authors",produces = { MediaType.APPLICATION_JSON_VALUE })
@Validated 
public class AuthorController {
	
	@Autowired
	IAuthorService authorService;
	@Autowired
	ModelMapper modelMapper;

	
	// -------------------Retrieve All Authors---------------------------------------------
	@ApiOperation(value = "Find all authors",notes = "Return all authors", response = List.class)
	@GetMapping()
	@Dto(AuthorDTO.class)
    public ResponseEntity<Page<Author>> findAll(
    		@ApiParam("The size of the page to be returned") @RequestParam(defaultValue = "10",required = false) Integer size, 
    		@ApiParam("Zero-based page index") @RequestParam(defaultValue = "0",required = false) Integer page) {

		Pageable pageable = new PageRequest(page, size);
		
        return ResponseEntity.ok(authorService.findAll(pageable));
    }
	
	
	// -------------------Create a Author-------------------------------------------
	@ApiOperation(value = "Create new Author", notes = "Creates new  Author. Returns created tag with id.",response = AuthorDTO.class)
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	@Dto(AuthorDTO.class)
    public ResponseEntity<Author> create(
    		@ApiParam(name = "Author", value = "JSON author to create", required = true)
    		@Valid @RequestBody AuthorCreationDTO authorNew) {
		Author author = modelMapper.map(authorNew, Author.class);
		authorService.insert(author);
        return ResponseEntity.status(HttpStatus.CREATED).body(author);
    }
	
	
	// -------------------Retrieve Single Author------------------------------------------
	@ApiOperation(value = "Get author by id", notes = "Returns author for id specified.",response = AuthorDTO.class)
	@GetMapping("/{id}")
	@Dto(AuthorDTO.class)
    public ResponseEntity<Author> findById(
    		@ApiParam(value = "Author id from which author object will retrieve", required = true)
    		@PathVariable(value = "id") Long id) throws EntityNotFoundException  {
        return ResponseEntity.ok(authorService.findById(id));
    }
	

	// ------------------- Update a Author ------------------------------------------------
	@ApiOperation(value = "Update a author",notes = "Update author by id and return it")
	@PutMapping(path = "/{id}",consumes = MediaType.APPLICATION_JSON_VALUE)
	@Dto(AuthorDTO.class)
    public ResponseEntity<Author> update(
    		@ApiParam(value = "Author id from which author object will update", required = true)
    		@PathVariable Long id, 
    		@Valid @RequestBody AuthorUpdateDTO authorUpdate) {
		
		Author author = modelMapper.map(authorUpdate, Author.class);
		author.setId(id);
		authorService.update(author);

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(author);
    }
	
	
	
	// ------------------- Delete a Author-----------------------------------------
	@ApiOperation(value = "Delete a author",notes = "Delete author for id specified.")
	@DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
		authorService.deleteById(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }
	
	
	
}

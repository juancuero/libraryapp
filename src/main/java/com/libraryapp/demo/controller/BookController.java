package com.libraryapp.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.libraryapp.demo.EntityNotFoundException;
import com.libraryapp.demo.config.Dto;
import com.libraryapp.demo.dto.BookCreationDTO;
import com.libraryapp.demo.dto.BookDTO;
import com.libraryapp.demo.dto.BookUpdateDTO;
import com.libraryapp.demo.entity.Author;
import com.libraryapp.demo.entity.Book;
import com.libraryapp.demo.entity.Editorial;
import com.libraryapp.demo.service.IAuthorService;
import com.libraryapp.demo.service.IBookService;
import com.libraryapp.demo.service.IEditorialService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

@RestController
@Api(tags = "Book Endpoint",value = "Book Management System", description = "Operations pertaining to book in Library Management System")
@RequestMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
@Validated 
@Slf4j
public class BookController {
	
	@Autowired
	IBookService bookService;
	@Autowired
	IEditorialService editorialService;
	@Autowired
	IAuthorService authorService;
	@Autowired
	ModelMapper modelMapper;
	
	
	// -------------------Retrieve All Books---------------------------------------------
	@ApiOperation(value = "Find all books",notes = "Return all books", response = List.class)
	@GetMapping(path = "/books")
	@Dto(BookDTO.class)
    public ResponseEntity<Page<Book>> findAll(
    		@ApiParam("The size of the page to be returned") @RequestParam(defaultValue = "10",required = false) Integer size, 
    		@ApiParam("Zero-based page index") @RequestParam(defaultValue = "0",required = false) Integer page) {

		Pageable pageable = new PageRequest(page, size);
		
        return ResponseEntity.ok(bookService.findAll(pageable));
    }
	
	// -------------------Retrieve All Books By Editorial---------------------------------------------
	@ApiOperation(value = "Find all books by editorial",notes = "Retrieve All Books By Editorial", response = List.class)
	@GetMapping(path = "/editorials/{editorialId}/books")
	@Dto(BookDTO.class)
    public ResponseEntity<Page<Book>> findByEditorial(
    		@ApiParam(value = "id of the editoral", required = true)
			@PathVariable(value = "editorialId") Long editorialId,
    		@ApiParam("The size of the page to be returned") @RequestParam(defaultValue = "10",required = false) Integer size, 
    		@ApiParam("Zero-based page index") @RequestParam(defaultValue = "0",required = false) Integer page) throws EntityNotFoundException {

		Pageable pageable = new PageRequest(page, size);
		
        return ResponseEntity.ok(bookService.findByEditorialId(editorialId, pageable));
    }
	
	// -------------------Retrieve Single Editorial------------------------------------------
	@ApiOperation(value = "Get book by id", notes = "Returns book for id specified.",response = BookDTO.class)
	@GetMapping("/books/{id}")
	@Dto(BookDTO.class)
    public ResponseEntity<Book> findById(
    		@ApiParam(value = "Book id from which tag object will retrieve", required = true)
    		@PathVariable(value = "id") Long id) throws EntityNotFoundException  {
        return ResponseEntity.ok(bookService.findById(id));
    }
	
	// -------------------Create a Book-------------------------------------------
	@ApiOperation(value = "Create new book", notes = "Creates new  book. Returns created book with id.",response = BookDTO.class)
	@PostMapping(path = "/editorials/{editorialId}/books")
	@Dto(BookDTO.class)
    public ResponseEntity<Book> create(
    		@ApiParam(value = "id of the editoral", required = true)
			@PathVariable(value = "editorialId") Long editorialId,
    		@ApiParam(name = "Book", value = "JSON book to create", required = true)
    		@Valid @RequestBody BookCreationDTO bookNew) throws EntityNotFoundException {
		
		log.info("inside method create() path: /editorials/{}/books",editorialId);
	    
		Book book = modelMapper.map(bookNew, Book.class);		
		
		Set<Author> authorsSet = authorService.findSomeByInId(bookNew.getAuthorIds());
	
		List<Author> authors = new ArrayList<>(authorsSet);
		
		book.setAuthors(authors);

		bookService.insert(editorialId,book);
		
        return ResponseEntity.status(HttpStatus.CREATED).body(book);
      
    }
	
	// ------------------- Update a Book ------------------------------------------------
	@ApiOperation(value = "Update a book",notes = "Update editorial by id and return it",response = BookDTO.class)
	@PutMapping(path = "/books/{id}",consumes = MediaType.APPLICATION_JSON_VALUE)
	@Dto(BookDTO.class)
    public ResponseEntity<Book> update(
    		@ApiParam(value = "id from which Book  object will update", required = true)
    		@PathVariable Long id, 
    		@Valid @RequestBody BookUpdateDTO bookUpdate) throws EntityNotFoundException {
		
		Book book = modelMapper.map(bookUpdate, Book.class);
		
		Set<Author> authorsSet = authorService.findSomeByInId(bookUpdate.getAuthorIds());
		
		List<Author> authors = new ArrayList<>(authorsSet);
		
		book.setAuthors(authors);
		
		Editorial editorial = editorialService.findById(bookUpdate.getEditorialId());
		book.setEditorial(editorial);
		
		bookService.update(id,book);

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(book);
    }
	
	
	// ------------------- Delete a Book-----------------------------------------
	@ApiOperation(value = "Delete a book",notes = "Delete book for id specified.")
	@DeleteMapping("/books/{id}")
    public ResponseEntity<?> delete(@ApiParam(value = "id of the book to delete", required = true)
							@PathVariable(value = "id") Long id) {
		bookService.deleteById(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }

	

}

package com.libraryapp.demo.controller;

import java.util.Arrays;
import java.util.List;
import java.lang.reflect.Type;


import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.MediaType;

import com.libraryapp.demo.EntityNotFoundException;
import com.libraryapp.demo.config.Dto;
import com.libraryapp.demo.dto.BookCreationDTO;
import com.libraryapp.demo.dto.BookDTO;
import com.libraryapp.demo.dto.EditorialCreationDTO;
import com.libraryapp.demo.dto.EditorialDTO;
import com.libraryapp.demo.dto.EditorialUpdateDTO;
import com.libraryapp.demo.entity.Book;
import com.libraryapp.demo.entity.Editorial;
import com.libraryapp.demo.service.IBookService;
import com.libraryapp.demo.service.IEditorialService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;




@RestController
@Api(tags = "Editorial Endpoint",value = "Editorial Management System", description = "Operations pertaining to editorial in Library Management System")
@RequestMapping(path = "/editorials",produces = { MediaType.APPLICATION_JSON_VALUE })
@Validated 
public class EditorialController {
	
	@Autowired
	IEditorialService editorialService;
	@Autowired
	IBookService bookService;
	@Autowired
	ModelMapper modelMapper;

	
	// -------------------Retrieve All Editorials---------------------------------------------
	@ApiOperation(value = "Find all tags",notes = "Return all tags", response = List.class)
	@GetMapping()
	@Dto(EditorialDTO.class)
    public ResponseEntity<Page<Editorial>> findAll(
    		@ApiParam("The size of the page to be returned") @RequestParam(defaultValue = "10",required = false) Integer size, 
    		@ApiParam("Zero-based page index") @RequestParam(defaultValue = "0",required = false) Integer page) {

		Pageable pageable = new PageRequest(page, size);
		
        return ResponseEntity.ok(editorialService.findAll(pageable));
    }
	
	
	// -------------------Create a Editorial-------------------------------------------
	@ApiOperation(value = "Create new editorial", notes = "Creates new  editorial. Returns created editorial with id.",response = EditorialDTO.class)
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	@Dto(EditorialDTO.class)
    public ResponseEntity<Editorial> create(
    		@ApiParam(name = "Editorial", value = "JSON editorial to create", required = true)
    		@Valid @RequestBody EditorialCreationDTO editorialNew) {
		Editorial editorial = modelMapper.map(editorialNew, Editorial.class);
		editorialService.insert(editorial);
        return ResponseEntity.status(HttpStatus.CREATED).body(editorial);
    }
	
	
	// -------------------Retrieve Single Editorial------------------------------------------
	@ApiOperation(value = "Get editorial by id", notes = "Returns editorial for id specified.",response = EditorialDTO.class)
	@GetMapping("/{id}")
	@Dto(EditorialDTO.class)
    public ResponseEntity<Editorial> findById(
    		@ApiParam(value = "Tag id from which tag object will retrieve", required = true)
    		@PathVariable(value = "id") Long id) throws EntityNotFoundException  {
        return ResponseEntity.ok(editorialService.findById(id));
    }
	

	// ------------------- Update a Tag ------------------------------------------------
	@ApiOperation(value = "Update a tag",notes = "Update editorial by id and return it",response = EditorialDTO.class)
	@PutMapping(path = "/{id}",consumes = MediaType.APPLICATION_JSON_VALUE)
	@Dto(EditorialDTO.class)
    public ResponseEntity<Editorial> update(
    		@ApiParam(value = "Editorial id from which editorial object will update", required = true)
    		@PathVariable Long id, 
    		@Valid @RequestBody EditorialUpdateDTO editorialUpdate) throws EntityNotFoundException {
		
		Editorial editorial = modelMapper.map(editorialUpdate, Editorial.class);
		editorialService.update(id,editorial);

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(editorial);
    }
	
	
	
	// ------------------- Delete a Editorial-----------------------------------------
	@ApiOperation(value = "Delete a editorial",notes = "Delete editorial for id specified.")
	@DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@ApiParam(value = "id of the editoral to delete", required = true)
							@PathVariable(value = "id") Long id) {
		editorialService.deleteById(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }
	
	
	
	
	
	
}

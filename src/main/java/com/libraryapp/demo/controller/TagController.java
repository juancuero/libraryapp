package com.libraryapp.demo.controller;

import java.util.Arrays;
import java.util.List;
import java.lang.reflect.Type;


import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.MediaType;

import com.libraryapp.demo.EntityNotFoundException;
import com.libraryapp.demo.config.Dto;
import com.libraryapp.demo.dto.TagCreationDTO;
import com.libraryapp.demo.dto.TagDTO;
import com.libraryapp.demo.dto.TagUpdateDTO;
import com.libraryapp.demo.entity.Tag;
import com.libraryapp.demo.service.ITagService;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;




@RestController
@Api(tags = "Tag Endpoint",value = "Tag Management System", description = "Operations pertaining to tag in Library Management System")
@RequestMapping(path = "/tags",produces = { MediaType.APPLICATION_JSON_VALUE })
@Validated 
public class TagController {
	
	@Autowired
	ITagService tagService;
	@Autowired
	ModelMapper modelMapper;

	
	// -------------------Retrieve All Tags---------------------------------------------
	@ApiOperation(value = "Find all tags",notes = "Return all tags", response = List.class)
	@GetMapping()
	@Dto(TagDTO.class)
    public ResponseEntity<Page<Tag>> findAll(
    		@ApiParam("The size of the page to be returned") @RequestParam(defaultValue = "10",required = false) Integer size, 
    		@ApiParam("Zero-based page index") @RequestParam(defaultValue = "0",required = false) Integer page) {

		Pageable pageable = new PageRequest(page, size);
		
        return ResponseEntity.ok(tagService.findAll(pageable));
    }
	
	
	// -------------------Create a Tag-------------------------------------------
	@ApiOperation(value = "Create new tag", notes = "Creates new  tag. Returns created tag with id.",response = TagDTO.class)
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	@Dto(TagDTO.class)
    public ResponseEntity<Tag> create(
    		@ApiParam(name = "Tag", value = "JSON tag to create", required = true)
    		@Valid @RequestBody TagCreationDTO tagNew) {
		Tag tag = modelMapper.map(tagNew, Tag.class);
		tagService.insert(tag);
        return ResponseEntity.status(HttpStatus.CREATED).body(tag);
    }
	
	// -------------------Create Multiple Tags-------------------------------------------
		@ApiOperation(value = "Create multiple tags", notes = "Creates new tags..",response = List.class)
		@PostMapping(path = "/insert-all",consumes = MediaType.APPLICATION_JSON_VALUE)
		@Dto(TagDTO.class)
	    public ResponseEntity<List<Tag>> createAll(
	    		@ApiParam(name = "Tags", value = "JSON of tags to create", required = true)
	    		@Valid @RequestBody List<TagCreationDTO> tagsDTO
	    		) {
			
			Type listType = new TypeToken<List<Tag>>(){}.getType();
			List<Tag> tags = modelMapper.map(tagsDTO,listType);
			tagService.insertAll(tags);
			
	        return ResponseEntity.status(HttpStatus.CREATED).body(tags);
	    }
	
	// -------------------Retrieve Single Tag------------------------------------------
	@ApiOperation(value = "Get tag by id", notes = "Returns tag for id specified.",response = TagDTO.class)
	@GetMapping("/{id}")
	@Dto(TagDTO.class)
    public ResponseEntity<Tag> findById(
    		@ApiParam(value = "Tag id from which tag object will retrieve", required = true)
    		@PathVariable(value = "id") Long id) throws EntityNotFoundException  {
        return ResponseEntity.ok(tagService.findById(id));
    }
	

	// ------------------- Update a Tag ------------------------------------------------
	@ApiOperation(value = "Update a tag",notes = "Update tag by id and return it")
	@PutMapping(path = "/{id}",consumes = MediaType.APPLICATION_JSON_VALUE)
	@Dto(TagDTO.class)
    public ResponseEntity<Tag> update(
    		@ApiParam(value = "Tag id from which tag object will update", required = true)
    		@PathVariable Long id, 
    		@Valid @RequestBody TagUpdateDTO tagUpdate) {
		
		Tag tag = modelMapper.map(tagUpdate, Tag.class);
		tag.setId(id);
		tagService.update(tag);

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(tag);
    }
	
	
	
	// ------------------- Delete a Tag-----------------------------------------
	@ApiOperation(value = "Delete a tag",notes = "Delete tag for id specified.")
	@DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
		tagService.deleteById(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }
	
	
	// ------------------- Delete All Tags-----------------------------
	@ApiOperation(value = "Delete all tags",notes = "Delete all tags.") 
	@DeleteMapping()
    public ResponseEntity<?> deleteAll() {
		tagService.deleteAll();
        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }
	
}

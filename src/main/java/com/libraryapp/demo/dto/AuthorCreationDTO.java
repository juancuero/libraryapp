package com.libraryapp.demo.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "Author DTO Creation", description = "Model to create a new Author")
public class AuthorCreationDTO {
	
	@NotNull(message = "{name.NotNull}")
    @Size(min = 3, message = "Name of author should have at least 3 characters")
    @ApiModelProperty(notes = "Name of author", example = "Juan Cuero")
	private String name;
}

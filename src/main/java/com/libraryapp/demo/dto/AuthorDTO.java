package com.libraryapp.demo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "Author DTO", description = "Author Model")
public class AuthorDTO {
	
	@ApiModelProperty(notes = "Id of the author", example = "1")
	private Long id;
	
	@ApiModelProperty(notes = "Author's name", example = "Juan Perez")
	private String name;

}

package com.libraryapp.demo.dto;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "Book DTO Creation", description = "Model to create a new Book")
public class BookCreationDTO {
	
	@NotNull(message = "{name.NotNull}")
    @Size(min = 4, message = "Title of tag should have at least 4 characters")
	@ApiModelProperty(notes = "Title of the editorial", example = "Norma",position = 1)
	private String title;
	
	@Size(min = 20, message = "Name of tag should have at least 20 characters")
	@ApiModelProperty(notes = "Description", example = "La editorial colombiana Norma se especializa en literatura infantil y escolar.",position = 2)
	private String description;
	
	@ApiModelProperty(notes = "Author's ids", example = "[2,3]",position = 3)
	private Set<Long> authorIds = new HashSet<>();
	
	
	
}

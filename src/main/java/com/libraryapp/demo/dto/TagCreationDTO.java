package com.libraryapp.demo.dto;


import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "Tag DTO Creation", description = "Model to create a new Tag")
public class TagCreationDTO {
	
	@NotNull(message = "{name.NotNull}")
    @Size(min = 4, message = "Name of tag should have at least 4 characters")
    @ApiModelProperty(notes = "Name of tag", example = "History")
	private String name;
}

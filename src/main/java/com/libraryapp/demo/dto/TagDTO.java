package com.libraryapp.demo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
@ApiModel(value = "Tag DTO", description = "Tag Model")
public class TagDTO {
	@ApiModelProperty(notes = "Id of the tag", example = "1")
	private Long id;
	@ApiModelProperty(notes = "Name of the tag", example = "History")
	private String name;

	
}

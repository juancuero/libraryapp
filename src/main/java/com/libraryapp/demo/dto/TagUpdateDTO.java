package com.libraryapp.demo.dto;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "Tag DTO Update", description = "Model to update a new Tag")
public class TagUpdateDTO {
	
	@NotNull(message = "{name.NotNull}")
	@ApiModelProperty(notes = "Name of tag", example = "Literature")
	private String name;
}

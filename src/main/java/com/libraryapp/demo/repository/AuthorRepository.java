package com.libraryapp.demo.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.libraryapp.demo.entity.Author;

public interface AuthorRepository extends JpaRepository<Author, Long> {
	
	Set<Author> findByIdIn(Set<Long> ids);
	
}

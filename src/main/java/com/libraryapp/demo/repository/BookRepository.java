package com.libraryapp.demo.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.domain.Pageable;
import com.libraryapp.demo.entity.Book;

public interface BookRepository extends JpaRepository<Book, Long> {
	Page<Book> findByEditorialId(Long editorialId, Pageable pageable);
}

package com.libraryapp.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.libraryapp.demo.entity.Editorial;



public interface EditorialRepository extends JpaRepository<Editorial, Long> {
}
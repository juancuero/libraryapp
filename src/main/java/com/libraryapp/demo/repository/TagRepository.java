package com.libraryapp.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.libraryapp.demo.entity.Tag;

public interface TagRepository extends JpaRepository<Tag, Long> {
}
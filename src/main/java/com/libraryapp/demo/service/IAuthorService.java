package com.libraryapp.demo.service;

import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.libraryapp.demo.EntityNotFoundException;
import com.libraryapp.demo.entity.Author;

public interface IAuthorService {
	
	Page<Author> findAll(Pageable pageable);
	
	Author findById(Long id) throws EntityNotFoundException;
	
	void insert(Author author);
	
	void update(Author author);
	
	void deleteById(Long id);
	
	Set<Author> findSomeByInId(Set<Long> ids);
}

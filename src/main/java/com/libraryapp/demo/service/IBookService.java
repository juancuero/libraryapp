package com.libraryapp.demo.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.libraryapp.demo.EntityNotFoundException;
import com.libraryapp.demo.entity.Book;



public interface IBookService {
	
	Page<Book> findAll(Pageable pageable);
	
	void insert(Long editorialId,Book book) throws EntityNotFoundException;
	
	Page<Book> findByEditorialId(Long editorialId,Pageable pageable) throws EntityNotFoundException;
	
	Book findById(Long id) throws EntityNotFoundException;
	
	void update(Long id,Book editorial) throws EntityNotFoundException;
	
	void deleteById(Long id);
}

package com.libraryapp.demo.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.libraryapp.demo.EntityNotFoundException;
import com.libraryapp.demo.entity.Editorial;

public interface IEditorialService {
	
	Page<Editorial> findAll(Pageable pageable);
	
	Editorial findById(Long id) throws EntityNotFoundException;
	
	void insert(Editorial editorial);
	
	void update(Long id,Editorial editorial) throws EntityNotFoundException;
	
	void deleteById(Long id);
	
}

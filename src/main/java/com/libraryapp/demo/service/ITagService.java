package com.libraryapp.demo.service;

import java.util.List;
import java.util.Optional;

import com.libraryapp.demo.EntityNotFoundException;
import com.libraryapp.demo.entity.Tag;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ITagService {
	
	Page<Tag> findAll(Pageable pageable);
	
	Tag findById(Long id) throws EntityNotFoundException;
	
	void insert(Tag tag);
	
	void update(Tag tag);
	
	void deleteById(Long id);
	
	void deleteAll();
	
	void insertAll(List<Tag> tags);
	
}

package com.libraryapp.demo.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.libraryapp.demo.EntityNotFoundException;
import com.libraryapp.demo.entity.Author;
import com.libraryapp.demo.repository.AuthorRepository;
import com.libraryapp.demo.service.IAuthorService;

@Service
public class AuthorServiceImpl implements IAuthorService {
	
	@Autowired
	private AuthorRepository authorRepository;

	@Override
	public Page<Author> findAll(Pageable pageable) {
		return authorRepository.findAll(pageable);
	}

	@Override
	public Author findById(Long id) throws EntityNotFoundException {
		Optional<Author> author = authorRepository.findById(id);
		if (!author.isPresent())
			 throw new EntityNotFoundException(Author.class, "id", id.toString());
		 return author.get();
	}

	@Override
	public void insert(Author author) {
		authorRepository.save(author);
	}

	@Override
	public void update(Author author) {
		authorRepository.save(author);
	}

	@Override
	public void deleteById(Long id) {
		authorRepository.deleteById(id);	
	}

	@Override
	public Set<Author> findSomeByInId(Set<Long> ids) {
		return authorRepository.findByIdIn(ids);
	}

}

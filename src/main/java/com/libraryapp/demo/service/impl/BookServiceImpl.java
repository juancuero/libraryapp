package com.libraryapp.demo.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.libraryapp.demo.EntityNotFoundException;
import com.libraryapp.demo.entity.Book;
import com.libraryapp.demo.entity.Editorial;
import com.libraryapp.demo.repository.BookRepository;
import com.libraryapp.demo.repository.EditorialRepository;
import com.libraryapp.demo.service.IBookService;
import com.libraryapp.demo.service.IEditorialService;

@Service
public class BookServiceImpl implements IBookService {
	
	@Autowired
	private BookRepository bookRespository;
	@Autowired
	IEditorialService editorialService;
	@Autowired
	IBookService bookService;
	
	@Override
	public void insert(Long editorialId,Book book) throws EntityNotFoundException {
		Editorial editorial = editorialService.findById(editorialId);
		book.setEditorial(editorial);
		bookRespository.save(book);
	}

	@Override
	public Page<Book> findByEditorialId(Long editorialId, Pageable pageable) throws EntityNotFoundException {
		Editorial editorial = editorialService.findById(editorialId);
		return bookRespository.findByEditorialId(editorialId, pageable);
	}

	@Override
	public Page<Book> findAll(Pageable pageable) {
		return bookRespository.findAll(pageable);
	}

	@Override
	public Book findById(Long id) throws EntityNotFoundException {
		Optional<Book> book = bookRespository.findById(id);
		if (!book.isPresent())
			 throw new EntityNotFoundException(Book.class, "id", id.toString());
		 return book.get();
	}

	@Override
	public void update(Long id, Book bookU) throws EntityNotFoundException {
		Book book = bookService.findById(id);
		bookU.setId(id);
		bookRespository.save(bookU);
	}

	@Override
	public void deleteById(Long id) {
		bookRespository.deleteById(id);		
	}

	

	
}

package com.libraryapp.demo.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.libraryapp.demo.EntityNotFoundException;
import com.libraryapp.demo.entity.Editorial;
import com.libraryapp.demo.repository.EditorialRepository;
import com.libraryapp.demo.service.IEditorialService;

@Service
public class EditorialServiceImpl implements IEditorialService {
	
	@Autowired
	private EditorialRepository editorialRespository;
	@Autowired
	IEditorialService editorialService;

	@Override
	public Page<Editorial> findAll(Pageable pageable) {
		
		return editorialRespository.findAll(pageable);
	}
	
	@Override
	public Editorial findById(Long id)  throws EntityNotFoundException {
		Optional<Editorial> editorial = editorialRespository.findById(id);
		if (!editorial.isPresent())
			 throw new EntityNotFoundException(Editorial.class, "id", id.toString());
		 return editorial.get();
	}
	

	@Override
	public void insert(Editorial editorial) {
		editorialRespository.save(editorial);
	}

	@Override
	public void update(Long id,Editorial editorialU) throws EntityNotFoundException {
		Editorial editorial = editorialService.findById(id);
		editorialU.setId(id);
		editorialRespository.save(editorialU);
	}

	@Override
	public void deleteById(Long id) {
		editorialRespository.deleteById(id);	
	}

}

package com.libraryapp.demo.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.libraryapp.demo.EntityNotFoundException;
import com.libraryapp.demo.entity.Tag;
import com.libraryapp.demo.repository.TagRepository;
import com.libraryapp.demo.service.ITagService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@Service
public class TagServiceImpl implements ITagService {
	
	@Autowired
	private TagRepository tagRespository;

	@Override
	public Page<Tag> findAll(Pageable pageable) {
		
		return tagRespository.findAll(pageable);
	}
	
	@Override
	public Tag findById(Long id)  throws EntityNotFoundException {
		Optional<Tag> tag = tagRespository.findById(id);
		if (!tag.isPresent())
			 throw new EntityNotFoundException(Tag.class, "id", id.toString());
		 return tag.get();
	}
	

	@Override
	public void insert(Tag tag) {
		tagRespository.save(tag);
	}

	@Override
	public void update(Tag tag) {
		tagRespository.save(tag);
	}

	@Override
	public void deleteById(Long id) {
		tagRespository.deleteById(id);	
	}

	@Override
	public void deleteAll() {
		tagRespository.deleteAll();
	}

	@Override
	public void insertAll(List<Tag> tags) {
		 tagRespository.saveAll(tags);
	}

	

}
